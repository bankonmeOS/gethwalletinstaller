# gethwalletinstaller

A bash script to automate the installation of the GETH Ethereum Go Blockchain wallet on Ubuntu 20.04LTS

This script will install Nodejs 10 Yarn Truffle and Ethereum GETH wallet dependencies then will download the Ethereum blockchain as it approaches 1 TeraByte as of 20210121

IMPORTANT!!!!!!

After the Ethereum testnet is installed write down your mnemonic then press 'Control C" to kill the node server and continue with the GETH install. I tried to script in a bash kill. TO DO: script in a bash process kill. 'Control C" after testnet loads suffices for now.

just finished downloading the Ethereum blockchain. Note: because Eth/privnet was created by root you will experience a permission issue after getting the entire chain. With user and group bankonme  

$ chmod -R Eth/privnet bankonme:bankonme 


# installation copy and paste tested on Ubuntu 20.04
sudo apt install git -y && git clone https://gitlab.com/bankonmeOS/gethwalletinstaller.git && cd gethwalletinstaller && chmod +x geth.install &&
./geth.install
